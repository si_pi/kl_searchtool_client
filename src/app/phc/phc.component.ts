/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
import { AfterContentInit, AfterViewInit, Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ICPC2Model } from '../models/ICPC2Model';
@Component({
  selector: 'app-phc',
  templateUrl: './phc.component.html',
  styleUrls: ['./phc.component.scss']
})
export class PhcComponent implements OnInit {

  constructor(private service: DataService) {}
  icpc2Arr$: ICPC2Model[] = [];
  
  selectedICPC2: ICPC2Model = new ICPC2Model();
  selectedIndex:number = -1;

  public searchString: string="";

  public showICPC3: boolean= false;
  public showSNOMEDCT: boolean= false;
  public showICD10: boolean= false;
  public showICPC2: boolean= true;

  public searchICPC3: boolean= false;
  public searchSNOMEDCT: boolean= false;
  public searchICD10: boolean= false;
  public searchICPC2: boolean= true;

  public codeSystem: string = "searchICPC2";

  public selectedKapitel = "";
  public selectedColor = 0;
  //public searchExklusive: boolean= false;

  ngOnInit(): void {
    this.onKey();
  }
  

  getICPC2s() {
    this.service.getICPC2Allgemein("").subscribe((data: any) => {
      console.log(data);
      this.icpc2Arr$ = data.content;
    });
  }
  selectICPC2(selected: ICPC2Model, i:number) {
    console.log('clicked ICPC2 ' + selected.id + " code: " + selected.icpc2);
    this.selectedIndex = i;
    this.selectedICPC2 = selected;
  }

  changeSearchInCode(e:any){
    this.codeSystem = e.target.id;
    switch (this.codeSystem){
      case "ICD10":
        this.showICD10 = true;
        break;
      case "SNOMEDCT":
        this.showSNOMEDCT = true;
        break;
      case "ICPC3":
        this.showICPC3 = true;
        break;
    }
    this.onKey();
  }

  translateFarbe(farbe:number){
    switch (farbe){
      case 1: //Prozeduren-codes
        return "#d6fdd0"
      case 2: //symptome
        return "#cccdfb"
      case 3: // Infektionen
        return "#fffea6"
      case 4: // Neubildungen
        return "#d5fefe"
      case 5: //Verletzungen
        return "#f2a0ca"
      case 6: //Angeborene Anomalien
         return "#f9cda1"         
      case 7: //Andere Diagnosen
        return "#a3cbfa"
      case 8: //Substanzen - Allergien, Unverträglichkeit
        return "#dd816d"
      case 9: // Gesundheitsprobleme und Risiken
        return "#f2b5a5"
      case 10: // Vorerkrankungen
        return "#f9e5d0"
      case 11: // Eingriffe
        return "#b8deb4"
      case 12: //Implantate
        return "#efefef"
      default:
        return ""
    }
  }

  

  onKey() {
    console.log("Value:"+ this.searchString);
    this.selectedICPC2 = new ICPC2Model();
    this.selectedIndex = -1;
    
    this.service.getSearchCode(this.searchString, this.codeSystem, this.selectedKapitel, this.selectedColor).subscribe((data: any) => {
      console.log(data);
      this.icpc2Arr$ = data;
      this.selectedICPC2 = new ICPC2Model();
      this.selectedIndex = -1;
    });

    /*this.service.getICPC2Allgemein(value).subscribe((data: any) => {
      console.log(data);
      this.icpc2Arr$ = data;
    });*/
    
  }

  clickedKapitel($event:any, kapitel : string)
  {
    let clickedElement = $event.target || $event.srcElement;
      //qclickedElement.className += " active";
      if( clickedElement.nodeName === "BUTTON" ) {
  
        let isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector(".active");
        // if a Button already has Class: .active
        if( isCertainButtonAlreadyActive ) {
          isCertainButtonAlreadyActive.classList.remove("active");
        }
  
        clickedElement.className += " active";
      }
    this.selectedKapitel = kapitel;
    this.onKey();
  }


  clickedColor($event:any, color:number){

      let clickedElement = $event.target || $event.srcElement;
      //qclickedElement.className += " active";
      if( clickedElement.nodeName === "BUTTON" ) {
  
        let isCertainButtonAlreadyActive = clickedElement.parentElement.querySelector(".active");
        // if a Button already has Class: .active
        if( isCertainButtonAlreadyActive ) {
          isCertainButtonAlreadyActive.classList.remove("active");
        }
  
        clickedElement.className += " active";
      }
  
    

    this.selectedColor = color;
    this.onKey();
  }

  clickedCode(icpc2:string, allgemeinUndUnspezifisch:any){
    this.searchString=icpc2;
    this.onKey();
    

    /*
    this.service.getICPC2Allgemein(icpc2).subscribe((data: any) => {
      this.selectedICPC2 = new ICPC2Model();
      //console.log(data);
      //this.searchString = icpc3 + " " + allgemeinUndUnspezifisch;
      this.searchString = icpc2;
      this.icpc2Arr$ = data;
    });*/
  }

}
