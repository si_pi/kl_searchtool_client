import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZweckbestimmungComponent } from './zweckbestimmung.component';

describe('ZweckbestimmungComponent', () => {
  let component: ZweckbestimmungComponent;
  let fixture: ComponentFixture<ZweckbestimmungComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZweckbestimmungComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZweckbestimmungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
