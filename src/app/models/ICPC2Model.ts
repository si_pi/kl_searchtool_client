/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
export class ICPC2Model {
  constructor(
    public id: string = '',
    public icpc2: string = '',
    public begriffe: string = '',
    //public exklusive?: Array<Exclusive>,
    public kriterien: string = '',
    public redFlags: string = '',
    public hinweise: string = '',
    public icpc3:string = '',
    public icd10:string='',
    public icd10Display:string='',
    public icd11:string='',
    public icd11Begriffe:string='',
    public snomedCT:string='',
    public snomedCTInternationalBrowser:string='',
    public farbe:number=0,
    public htmlfarbe:string='blue'
  ) {}
}
