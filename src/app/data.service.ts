/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ICPC2Model } from './models/ICPC2Model';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

//KL
const icpc2Url: string = 'https://primarycarecodes.kl.ac.at:443/api/icpc2';
const secUrl: string = 'https://primarycarecodes.kl.ac.at:443/api/sec';
//const icpc2Url: string = 'https://localhost:443/api/icpc2';
//const secUrl: string = 'https://localhost:443/api/sec';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Allow-Methods': 'GET',
    'Access-Control-Allow-Origin': '*'
  }),
};

@Injectable({
  providedIn: 'root',
})
export class DataService {

  USER_NAME_SESSION_ATTRIBUTE_NAME:string = 'token'

  constructor(private http: HttpClient, private router: Router) {}

  public getICPC2s(): Observable<ICPC2Model[]> {
    console.log('getICPC2s called');
    return this.http
      .get<ICPC2Model[]>(icpc2Url+"/search")
      .pipe(catchError(this.handleError('getICPC2s', [])));
  }
  public getICPC2Allgemein(allgemein: string): Observable<ICPC2Model[]> {
    return this.http
      .get<ICPC2Model[]>(icpc2Url +"/search?allgemein="+ allgemein)
      .pipe(catchError(this.handleError('getICPC2Detail', [])));
  }
  public getSearchCode(allgemein: string, codeSystem:string, selectedKapitel:string, selectedColor:number): Observable<ICPC2Model[]> {
    return this.http
      .get<ICPC2Model[]>(icpc2Url +"/search?term="+ allgemein + "&codeSystem="+codeSystem+"&kapitel="+selectedKapitel+"&color="+selectedColor)
      .pipe(catchError(this.handleError('getICPC2Detail', [])));
  }
  public getICPC2Code(code: string): Observable<ICPC2Model[]> {
    return this.http
      .get<ICPC2Model[]>(icpc2Url +"/search?code="+code)
      .pipe(catchError(this.handleError('getICPC2Detail', [])));
  }
  public uploadFile(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${secUrl}/upload`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }

  public activateFile(file:string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    const req = new HttpRequest('POST', `${secUrl}/activatefile`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
    /*return this.http
    .post<any>(secUrl +"/activatefile",formData, {
      reportProgress: true,
      responseType: 'json'
    })
    .pipe(catchError(this.handleError('getICPC2Detail', [])));*/
  }

  /*
  public activateFile(file:string): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${secUrl}/activatefile`, formData, {
      reportProgress: true,
      responseType: 'json'
    });

    return this.http.request(req);
  }
*/

  getFiles(): Observable<any> {
    return this.http.get(`${icpc2Url}/files`)
    .pipe(catchError(this.handleError('getICPC2s', [])));;
  }

  login(model:any):Observable<any> {
    return this.http.post<Observable<boolean>>(`${icpc2Url}/login`, {
      userName: model.username,
      password: model.password
  })
  .pipe(catchError(this.handleError('getICPC2s', [])));
  }

  
  logout() {
    sessionStorage.removeItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME);
  }

  /*
  registerSuccessfulLogin(username: String, password: any) {
    sessionStorage.setItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME, password)
  }*/
  
  isUserLoggedIn():boolean {
    let user = sessionStorage.getItem(this.USER_NAME_SESSION_ATTRIBUTE_NAME)
    if (user === null) return false
    return true
  }
  
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      //this.router.navigate(['']); 
      return of(result as T);
    };
  }
}
