/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataService } from '../data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  credentials = {username: '', password: ''};

  model: any = {};

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private dataservice: DataService
    
  ) { }

  ngOnInit() {
      sessionStorage.setItem('token', '');
  }

  

  login() {
    this.dataservice.login(this.model).subscribe(
      (isValid) => {
        if (isValid) {
          sessionStorage.setItem(
            'token', 
            btoa(this.model.username + ':' + this.model.password)
          );
          this.router.navigate(['']);
        } else {
            alert("Authentication failed.")
            this.router.navigate(['/admin']);
        }
    }
    );
  }
  
  logout(){
    
    sessionStorage.removeItem(
      'token'
    );
  }

}
