/*
 * Copyright (c) 2023 Mag. DI Simon Pirker
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.
 */
import { Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import { DataService } from '../data.service';
import { ICPC2Model } from '../models/ICPC2Model';
@Component({
  selector: 'app-icpc2details',
  templateUrl: './icpc2details.component.html',
  styleUrls: ['./icpc2details.component.scss']
})
export class Icpc2detailsComponent implements OnInit {

  constructor() { 


  }
 

  ngOnInit(): void {
    console.log("model code "+this.icpc2model.icpc2)
  }

  @Input()
  icpc2model: ICPC2Model = new ICPC2Model();

}
